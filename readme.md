# Ultimate Backpacks - REQUIRES ANOMALY 1.5.2

## Features
- Upgrade backpacks for additional inventory weight
- Open backpacks and store items inside!
- Holding the modifier key (set in mcm) while taking an item will put the item directly into your worn backpack
- Holding the modifier key and pressing the inventory key will directly open your backpack
- Backpacks can have a weight limit (MCM setting) (works like backpack volume)
- Backpacks can reduce the weight of items carried inside (like in project zomboid) (MCM setting)
- Weight limit and weight reducement can be upgraded at your local mechanic
- Creating a stash with your backpack will put the items directly into your stash
- Having radioactive items in your backpack will also poison you
- FDDA Support but required!

## !!!!!!!!!! IMPORTANT READ ME !!!!!!
- FDDA REQUIRED
- "Upgradeable Backpack" and "Open Backpacks" are not compatible with this mod as this mod basically enhances those mods. If you used "open backpacks" then empty your backpacks before switching to this mod! Consider saving before adding this mod to your load order.

## Disclaimer
This addon uses "STALKER UPGRADEABLE BACKPACK by AeroFW" and "Open Backpacks 1.3 by Junx" as base mods, merges them and adds additional functionality. If any of the mod
authors have something against me uploading this mod than please reach out to me.

## Known Issues
 - Changing of MCM-Settings results in some values not updating accordingly -> Save and load the game after changing settings
 - Backpack and inventory weight sometimes not updating accordingly -> Reopen the inventory / backpack once or twice will fix the issue for now
 - "MOUSE OVER TRANSFER ITEMS" mod works but should not be used to transfer items into your backpack or it will start spamming your inventory with plastic jars
 - GAMMA: Item upgrades not showing on backpack hover (upgrades still working!!!) [(if someone knows why this is happening pls reach out to me)]
 - EFP: Not tested

## Future Plans
- Free for good ideas

## Changelog
[06-09-2022] 
- Uploaded addon

[07-09-2022] 
- FDDA support is now optional (not required anymore, remember to turn off the fdda flag in mcm!)
- Open/Close Backpack string is now translateable in the xml files
- Crouching on open is now optional, look in mcm